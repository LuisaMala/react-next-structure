import {Header, Footer} from "./elements"
import { Hero } from "./sections/Hero/Hero"


const Layout = ({ children, global, pageContext }) => {
  const { navbar, footer, notificationBanner } = global


  return (
    <>
      <Header navbar={navbar} pageContext={pageContext} />
      {children}
      <Hero/>
      <Footer footer={footer} /> 
    </>
  )
}

export default Layout
