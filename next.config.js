const path = require('path')

module.exports = {
  i18n: {
    locales: ["en"],
    defaultLocale: "en",
  },
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
  async rewrites() {
    return [
      {
        source: "/api/:path*",
        destination: "https://reeftechnology-dev.reefplatform.com:8443/:path*",
      },
    ];
  },
  async redirects() {
    return [
      {
        source: "/privacy-policy",
        destination: "/privacy",
        permanent: true,
      },
      {
        source: "/legacy-parking",
        destination: "/parking-solutions/our-parking-history",
        permanent: true,
      },
      {
        source: "/app",
        destination: "/parking-solutions/download-reef-mobile-app",
        permanent: true,
      },
      {
        source: "/app/help",
        destination: "/parking-solutions/reef-mobile-app/help",
        permanent: true,
      },
      {
        source: "/vip-card",
        destination: "/parking-solutions/reef-mobile-app/vip-card",
        permanent: true,
      },
      {
        source: "/ultimate-guide-grow-food-delivery-business",
        destination:
          "/kitchens/ultimate-guide-to-growing-your-food-delivery-business",
        permanent: true,
      },
      {
        source: "/app/eula",
        destination: "/parking-services/reef-mobile-app/eula",
        permanent: true,
      },
      {
        source: "/app/reef-mobile-privacy-notice",
        destination: "/parking-services/reef-mobile-app/privacy",
        permanent: true,
      },
      {
        source: "/ecosystem-terms",
        destination: "/parking-services/parcs-equipment/terms",
        permanent: true,
      },
      {
        source: "/terms-conditions",
        destination: "/impact/nbrhd-restaurant-development-program/terms",
        permanent: true,
      },
      {
        source: "/nbrhd-restaurant-development-program",
        destination: "/impact/nbrhd-restaurant-development-program",
        permanent: true,
      },
      {
        source: "/growthmarketing",
        destination: "/growth-marketing",
        permanent: true,
      },
      {
        source: "/landlord-calculator",
        destination: "/real-estate/property-calculator",
        permanent: true,
      },
      {
        source: "/retail-and-ecommerce",
        destination: "/retail",
        permanent: true,
      },
      {
        source: "/fulfillment-and-logistics",
        destination: "/fulfillment-logistics",
        permanent: true,
      },
      {
        source: "/applications",
        destination: "/real-estate/property-applications",
        permanent: true,
      },
      {
        source: "/equal-employment-opportunity-policy",
        destination: "/equal-opportunity-employment/policy",
        permanent: true,
      },
      {
        source: "/equal-employment-opportunity-resources",
        destination: "/equal-opportunity-employment/resources",
        permanent: true,
      },
      {
        source: "/pay-transparency-nondiscrimination-provision",
        destination: "/equal-opportunity-employment/pay-transparency",
        permanent: true,
      },
      {
        source: "/get-reef-locations",
        destination: "/getREEF/locations-brands",
        permanent: true,
      },
      {
        source: "/uk-kitchens",
        destination: "/hospitality/uk-virtual-kitchen",
        permanent: true,
      },
      {
        source:
          "/corporate/iii-points-festival-announces-exclusive-food-provider-partnership-with-reef",
        destination:
          "/kitchens/iii-points-festival-announces-exclusive-food-provider-partnership-with-reef",
        permanent: true,
      },
      {
        source: "/solutions/parking",
        destination: "/parking-solutions",
        permanent: true,
      },
      {
        source: "/reef-studio-internal-form",
        destination: "/",
        permanent: true,
      },
      {
        source: "/job-fair",
        destination: "/",
        permanent: true,
      },
      {
        source: "/parking-services/parcs-equipment/terms",
        destination: "/parking-solutions/parcs-equipment/terms",
        permanent: true,
      },
      {
        source:
          "/parking-solutions/reef-technology-and-tiba-parking-systems%e2%80%afpartner-to-create-a-powerful-data-driven-platform-for-parking-operators-and-landlords",
        destination:
          "/parking-solutions/reef-technology-and-tiba-parking-systems-partner-to-create-a-powerful-data-driven-platform-for-parking-operators-and-landlords",
        permanent: true,
      },
      {
        source: "/church-calc",
        destination: "/real-estate/property-calculator",
        permanent: true,
      },
    ];
  },
};

