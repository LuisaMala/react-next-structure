import App from "next/app"
import Head from "next/head"
import ErrorPage from "next/error"
import { useRouter } from "next/router"
import { DefaultSeo } from "next-seo"
import { getStrapiMedia } from "utils/media"
import { getGlobalData } from "utils/api"
import { Provider } from 'react-redux'
import store from '../store'

import 'bootstrap/dist/css/bootstrap.css'
import "@/styles/fonts.css"
import "@/sass/style.scss"


const MyApp = ({ Component, pageProps }) => {
  // Extract the data we need
  const { global } = pageProps

  if (global == null) {
    return <ErrorPage statusCode={404} />
  }

  // const { metadata } = global

  return (

    <Provider store={store}>

      {/* Favicon */}
      <Head>
        {/* <link rel="shortcut icon" href={getStrapiMedia(global.favicon.url)} /> */}
      </Head>
      {/* Global site metadata */}
      {/* <DefaultSeo
        titleTemplate={`%s | ${global.metaTitleSuffix}`}
        title="Page"
        description={metadata.metaDescription}
        openGraph={{
          images: Object.values(metadata.shareImage.formats).map((image) => {
            return {
              url: getStrapiMedia(image.url),
              width: image.width,
              height: image.height,
            }
          }),
        }}
        twitter={{
          cardType: metadata.twitterCardType,
          handle: metadata.twitterUsername,
        }}
      /> */}
      {/* Display the content */}
      <Component {...pageProps} />
    </Provider>
  )
}

// getInitialProps disables automatic static optimization for pages that don't
// have getStaticProps. So [[...slug]] pages still get SSG.
// Hopefully we can replace this with getStaticProps once this issue is fixed:
// https://github.com/vercel/next.js/discussions/10949
MyApp.getInitialProps = async (appContext) => {
  // Calls page's `getInitialProps` and fills `appProps.pageProps`
  const appProps = await App.getInitialProps(appContext)
  const globalLocale = await getGlobalData(appContext.router.locale)

  return {
    ...appProps,
    pageProps: {
      global: globalLocale,
    },
  }
}

export default MyApp
