module.exports = {
  siteUrl: process.env.NEXT_PUBLIC_URL || 'https://reeftechnology.com',
  generateRobotsTxt: true,
  exclude: ['/article', '/posts-sitemap.xml'],
  robotsTxtOptions: {
    additionalSitemaps: [
      `${process.env.NEXT_PUBLIC_URL}/posts-sitemap.xml`,
    ],
  },
}