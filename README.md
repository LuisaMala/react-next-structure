# Next/React frontend #

Esta estructura es la base para dar inicio a un proyecto de Next/React con strapi como fuente de datos.

### Ejecución del proyecto ###

* Instalar las dependencias
**`yarn`**

* Ejecutar en desarrollo
**`yarn dev`**

* Ejecutar en producción
**`yarn build`**
**`yarn start`**
