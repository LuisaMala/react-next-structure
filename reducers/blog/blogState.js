import {createSlice} from '@reduxjs/toolkit';

const blogState = createSlice({
  name: "blogState",
  initialState: {
    blog: [],
    blogByCategory: [],
  },
  reducers: {
    setBlog(state, { payload }) {
      state.blog = payload
    },
    getBlogByCategory(state, { payload }) {
      state.blogByCategory = state.blog.filter(
        (item) => item.name === payload
      )
    }
  },
});

const BlogReducer = blogState.reducer;
const { getBlogByCategory, setBlog } = blogState.actions;

export { BlogReducer, getBlogByCategory, setBlog };