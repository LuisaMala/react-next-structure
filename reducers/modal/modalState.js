import {createSlice} from '@reduxjs/toolkit'

const modalState = createSlice({
  name: 'modalState',
  initialState: {
    isActive: false,
  },
  reducers: {
    setState(state, action) {
      state.isActive = action.payload;
    }
  }
})

const ModalReducer = modalState.reducer
const { setState } = modalState.actions

export { ModalReducer, setState };