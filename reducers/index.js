import {combineReducers} from 'redux'

import {ModalReducer} from './modal/modalState'
import {BlogReducer} from './blog/blogState'

// add all reducers

const rootReducer = combineReducers({
  ModalReducer,
  BlogReducer,
});

export default rootReducer