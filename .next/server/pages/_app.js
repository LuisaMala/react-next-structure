/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./node_modules/next/app.js":
/*!**********************************!*\
  !*** ./node_modules/next/app.js ***!
  \**********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("module.exports = __webpack_require__(/*! ./dist/pages/_app */ \"./node_modules/next/dist/pages/_app.js\")\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvbmV4dC9hcHAuanMuanMiLCJtYXBwaW5ncyI6IkFBQUEsdUdBQTZDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vcXVpbm4vLi9ub2RlX21vZHVsZXMvbmV4dC9hcHAuanM/ZjAxNiJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vZGlzdC9wYWdlcy9fYXBwJylcbiJdLCJuYW1lcyI6W10sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./node_modules/next/app.js\n");

/***/ }),

/***/ "./node_modules/next/dist/pages/_app.js":
/*!**********************************************!*\
  !*** ./node_modules/next/dist/pages/_app.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", ({\n  value: true\n}));\nObject.defineProperty(exports, \"AppInitialProps\", ({\n  enumerable: true,\n  get: function () {\n    return _utils.AppInitialProps;\n  }\n}));\nObject.defineProperty(exports, \"NextWebVitalsMetric\", ({\n  enumerable: true,\n  get: function () {\n    return _utils.NextWebVitalsMetric;\n  }\n}));\nexports.default = void 0;\n\nvar _react = _interopRequireDefault(__webpack_require__(/*! react */ \"react\"));\n\nvar _utils = __webpack_require__(/*! ../shared/lib/utils */ \"../shared/lib/utils\");\n\nfunction asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {\n  try {\n    var info = gen[key](arg);\n    var value = info.value;\n  } catch (error) {\n    reject(error);\n    return;\n  }\n\n  if (info.done) {\n    resolve(value);\n  } else {\n    Promise.resolve(value).then(_next, _throw);\n  }\n}\n\nfunction _asyncToGenerator(fn) {\n  return function () {\n    var self = this,\n        args = arguments;\n    return new Promise(function (resolve, reject) {\n      var gen = fn.apply(self, args);\n\n      function _next(value) {\n        asyncGeneratorStep(gen, resolve, reject, _next, _throw, \"next\", value);\n      }\n\n      function _throw(err) {\n        asyncGeneratorStep(gen, resolve, reject, _next, _throw, \"throw\", err);\n      }\n\n      _next(undefined);\n    });\n  };\n}\n\nfunction _interopRequireDefault(obj) {\n  return obj && obj.__esModule ? obj : {\n    default: obj\n  };\n}\n\nfunction _appGetInitialProps() {\n  _appGetInitialProps =\n  /**\n  * `App` component is used for initialize of pages. It allows for overwriting and full control of the `page` initialization.\n  * This allows for keeping state between navigation, custom error handling, injecting additional data.\n  */\n  _asyncToGenerator(function* ({\n    Component,\n    ctx\n  }) {\n    const pageProps = yield (0, _utils).loadGetInitialProps(Component, ctx);\n    return {\n      pageProps\n    };\n  });\n  return _appGetInitialProps.apply(this, arguments);\n}\n\nfunction appGetInitialProps(_) {\n  return _appGetInitialProps.apply(this, arguments);\n}\n\nclass App extends _react.default.Component {\n  render() {\n    const {\n      Component,\n      pageProps\n    } = this.props;\n    return /*#__PURE__*/_react.default.createElement(Component, Object.assign({}, pageProps));\n  }\n\n}\n\nApp.origGetInitialProps = appGetInitialProps;\nApp.getInitialProps = appGetInitialProps;\nexports.default = App;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvbmV4dC9kaXN0L3BhZ2VzL19hcHAuanMuanMiLCJtYXBwaW5ncyI6IkFBQWE7O0FBQ2JBLDhDQUE2QztBQUN6Q0csRUFBQUEsS0FBSyxFQUFFO0FBRGtDLENBQTdDO0FBR0FILG1EQUFrRDtBQUM5Q0ksRUFBQUEsVUFBVSxFQUFFLElBRGtDO0FBRTlDQyxFQUFBQSxHQUFHLEVBQUUsWUFBVztBQUNaLFdBQU9DLE1BQU0sQ0FBQ0MsZUFBZDtBQUNIO0FBSjZDLENBQWxEO0FBTUFQLHVEQUFzRDtBQUNsREksRUFBQUEsVUFBVSxFQUFFLElBRHNDO0FBRWxEQyxFQUFBQSxHQUFHLEVBQUUsWUFBVztBQUNaLFdBQU9DLE1BQU0sQ0FBQ0UsbUJBQWQ7QUFDSDtBQUppRCxDQUF0RDtBQU1BTixlQUFBLEdBQWtCLEtBQUssQ0FBdkI7O0FBQ0EsSUFBSVEsTUFBTSxHQUFHQyxzQkFBc0IsQ0FBQ0MsbUJBQU8sQ0FBQyxvQkFBRCxDQUFSLENBQW5DOztBQUNBLElBQUlOLE1BQU0sR0FBR00sbUJBQU8sQ0FBQyxnREFBRCxDQUFwQjs7QUFDQSxTQUFTQyxrQkFBVCxDQUE0QkMsR0FBNUIsRUFBaUNDLE9BQWpDLEVBQTBDQyxNQUExQyxFQUFrREMsS0FBbEQsRUFBeURDLE1BQXpELEVBQWlFQyxHQUFqRSxFQUFzRUMsR0FBdEUsRUFBMkU7QUFDdkUsTUFBSTtBQUNBLFFBQUlDLElBQUksR0FBR1AsR0FBRyxDQUFDSyxHQUFELENBQUgsQ0FBU0MsR0FBVCxDQUFYO0FBQ0EsUUFBSWpCLEtBQUssR0FBR2tCLElBQUksQ0FBQ2xCLEtBQWpCO0FBQ0gsR0FIRCxDQUdFLE9BQU9tQixLQUFQLEVBQWM7QUFDWk4sSUFBQUEsTUFBTSxDQUFDTSxLQUFELENBQU47QUFDQTtBQUNIOztBQUNELE1BQUlELElBQUksQ0FBQ0UsSUFBVCxFQUFlO0FBQ1hSLElBQUFBLE9BQU8sQ0FBQ1osS0FBRCxDQUFQO0FBQ0gsR0FGRCxNQUVPO0FBQ0hxQixJQUFBQSxPQUFPLENBQUNULE9BQVIsQ0FBZ0JaLEtBQWhCLEVBQXVCc0IsSUFBdkIsQ0FBNEJSLEtBQTVCLEVBQW1DQyxNQUFuQztBQUNIO0FBQ0o7O0FBQ0QsU0FBU1EsaUJBQVQsQ0FBMkJDLEVBQTNCLEVBQStCO0FBQzNCLFNBQU8sWUFBVztBQUNkLFFBQUlDLElBQUksR0FBRyxJQUFYO0FBQUEsUUFBaUJDLElBQUksR0FBR0MsU0FBeEI7QUFDQSxXQUFPLElBQUlOLE9BQUosQ0FBWSxVQUFTVCxPQUFULEVBQWtCQyxNQUFsQixFQUEwQjtBQUN6QyxVQUFJRixHQUFHLEdBQUdhLEVBQUUsQ0FBQ0ksS0FBSCxDQUFTSCxJQUFULEVBQWVDLElBQWYsQ0FBVjs7QUFDQSxlQUFTWixLQUFULENBQWVkLEtBQWYsRUFBc0I7QUFDbEJVLFFBQUFBLGtCQUFrQixDQUFDQyxHQUFELEVBQU1DLE9BQU4sRUFBZUMsTUFBZixFQUF1QkMsS0FBdkIsRUFBOEJDLE1BQTlCLEVBQXNDLE1BQXRDLEVBQThDZixLQUE5QyxDQUFsQjtBQUNIOztBQUNELGVBQVNlLE1BQVQsQ0FBZ0JjLEdBQWhCLEVBQXFCO0FBQ2pCbkIsUUFBQUEsa0JBQWtCLENBQUNDLEdBQUQsRUFBTUMsT0FBTixFQUFlQyxNQUFmLEVBQXVCQyxLQUF2QixFQUE4QkMsTUFBOUIsRUFBc0MsT0FBdEMsRUFBK0NjLEdBQS9DLENBQWxCO0FBQ0g7O0FBQ0RmLE1BQUFBLEtBQUssQ0FBQ2dCLFNBQUQsQ0FBTDtBQUNILEtBVE0sQ0FBUDtBQVVILEdBWkQ7QUFhSDs7QUFDRCxTQUFTdEIsc0JBQVQsQ0FBZ0N1QixHQUFoQyxFQUFxQztBQUNqQyxTQUFPQSxHQUFHLElBQUlBLEdBQUcsQ0FBQ0MsVUFBWCxHQUF3QkQsR0FBeEIsR0FBOEI7QUFDakN6QixJQUFBQSxPQUFPLEVBQUV5QjtBQUR3QixHQUFyQztBQUdIOztBQUNELFNBQVNFLG1CQUFULEdBQStCO0FBQzNCQSxFQUFBQSxtQkFBbUI7QUFBRztBQUMxQjtBQUNBO0FBQ0E7QUFBSVYsRUFBQUEsaUJBQWlCLENBQUMsV0FBVTtBQUFFVyxJQUFBQSxTQUFGO0FBQWNDLElBQUFBO0FBQWQsR0FBVixFQUFnQztBQUM5QyxVQUFNQyxTQUFTLEdBQUcsTUFBTSxDQUFDLEdBQUdqQyxNQUFKLEVBQVlrQyxtQkFBWixDQUFnQ0gsU0FBaEMsRUFBMkNDLEdBQTNDLENBQXhCO0FBQ0EsV0FBTztBQUNIQyxNQUFBQTtBQURHLEtBQVA7QUFHSCxHQUxnQixDQUhqQjtBQVNBLFNBQU9ILG1CQUFtQixDQUFDTCxLQUFwQixDQUEwQixJQUExQixFQUFnQ0QsU0FBaEMsQ0FBUDtBQUNIOztBQUNELFNBQVNXLGtCQUFULENBQTRCQyxDQUE1QixFQUErQjtBQUMzQixTQUFPTixtQkFBbUIsQ0FBQ0wsS0FBcEIsQ0FBMEIsSUFBMUIsRUFBZ0NELFNBQWhDLENBQVA7QUFDSDs7QUFDRCxNQUFNYSxHQUFOLFNBQWtCakMsTUFBTSxDQUFDRCxPQUFQLENBQWU0QixTQUFqQyxDQUEyQztBQUN2Q08sRUFBQUEsTUFBTSxHQUFHO0FBQ0wsVUFBTTtBQUFFUCxNQUFBQSxTQUFGO0FBQWNFLE1BQUFBO0FBQWQsUUFBNkIsS0FBS00sS0FBeEM7QUFDQSxXQUFPLGFBQWNuQyxNQUFNLENBQUNELE9BQVAsQ0FBZXFDLGFBQWYsQ0FBNkJULFNBQTdCLEVBQXdDckMsTUFBTSxDQUFDK0MsTUFBUCxDQUFjLEVBQWQsRUFDMURSLFNBRDBELENBQXhDLENBQXJCO0FBRUg7O0FBTHNDOztBQU8zQ0ksR0FBRyxDQUFDSyxtQkFBSixHQUEwQlAsa0JBQTFCO0FBQ0FFLEdBQUcsQ0FBQ00sZUFBSixHQUFzQlIsa0JBQXRCO0FBQ0F2QyxlQUFBLEdBQWtCeUMsR0FBbEIiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9xdWlubi8uL25vZGVfbW9kdWxlcy9uZXh0L2Rpc3QvcGFnZXMvX2FwcC5qcz8wNzk1Il0sInNvdXJjZXNDb250ZW50IjpbIlwidXNlIHN0cmljdFwiO1xuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gICAgdmFsdWU6IHRydWVcbn0pO1xuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiQXBwSW5pdGlhbFByb3BzXCIsIHtcbiAgICBlbnVtZXJhYmxlOiB0cnVlLFxuICAgIGdldDogZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiBfdXRpbHMuQXBwSW5pdGlhbFByb3BzO1xuICAgIH1cbn0pO1xuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiTmV4dFdlYlZpdGFsc01ldHJpY1wiLCB7XG4gICAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgICBnZXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gX3V0aWxzLk5leHRXZWJWaXRhbHNNZXRyaWM7XG4gICAgfVxufSk7XG5leHBvcnRzLmRlZmF1bHQgPSB2b2lkIDA7XG52YXIgX3JlYWN0ID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChyZXF1aXJlKFwicmVhY3RcIikpO1xudmFyIF91dGlscyA9IHJlcXVpcmUoXCIuLi9zaGFyZWQvbGliL3V0aWxzXCIpO1xuZnVuY3Rpb24gYXN5bmNHZW5lcmF0b3JTdGVwKGdlbiwgcmVzb2x2ZSwgcmVqZWN0LCBfbmV4dCwgX3Rocm93LCBrZXksIGFyZykge1xuICAgIHRyeSB7XG4gICAgICAgIHZhciBpbmZvID0gZ2VuW2tleV0oYXJnKTtcbiAgICAgICAgdmFyIHZhbHVlID0gaW5mby52YWx1ZTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICByZWplY3QoZXJyb3IpO1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmIChpbmZvLmRvbmUpIHtcbiAgICAgICAgcmVzb2x2ZSh2YWx1ZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgUHJvbWlzZS5yZXNvbHZlKHZhbHVlKS50aGVuKF9uZXh0LCBfdGhyb3cpO1xuICAgIH1cbn1cbmZ1bmN0aW9uIF9hc3luY1RvR2VuZXJhdG9yKGZuKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgc2VsZiA9IHRoaXMsIGFyZ3MgPSBhcmd1bWVudHM7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbihyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgICAgIHZhciBnZW4gPSBmbi5hcHBseShzZWxmLCBhcmdzKTtcbiAgICAgICAgICAgIGZ1bmN0aW9uIF9uZXh0KHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgYXN5bmNHZW5lcmF0b3JTdGVwKGdlbiwgcmVzb2x2ZSwgcmVqZWN0LCBfbmV4dCwgX3Rocm93LCBcIm5leHRcIiwgdmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZnVuY3Rpb24gX3Rocm93KGVycikge1xuICAgICAgICAgICAgICAgIGFzeW5jR2VuZXJhdG9yU3RlcChnZW4sIHJlc29sdmUsIHJlamVjdCwgX25leHQsIF90aHJvdywgXCJ0aHJvd1wiLCBlcnIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgX25leHQodW5kZWZpbmVkKTtcbiAgICAgICAgfSk7XG4gICAgfTtcbn1cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7XG4gICAgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHtcbiAgICAgICAgZGVmYXVsdDogb2JqXG4gICAgfTtcbn1cbmZ1bmN0aW9uIF9hcHBHZXRJbml0aWFsUHJvcHMoKSB7XG4gICAgX2FwcEdldEluaXRpYWxQcm9wcyA9IC8qKlxuICogYEFwcGAgY29tcG9uZW50IGlzIHVzZWQgZm9yIGluaXRpYWxpemUgb2YgcGFnZXMuIEl0IGFsbG93cyBmb3Igb3ZlcndyaXRpbmcgYW5kIGZ1bGwgY29udHJvbCBvZiB0aGUgYHBhZ2VgIGluaXRpYWxpemF0aW9uLlxuICogVGhpcyBhbGxvd3MgZm9yIGtlZXBpbmcgc3RhdGUgYmV0d2VlbiBuYXZpZ2F0aW9uLCBjdXN0b20gZXJyb3IgaGFuZGxpbmcsIGluamVjdGluZyBhZGRpdGlvbmFsIGRhdGEuXG4gKi8gX2FzeW5jVG9HZW5lcmF0b3IoZnVuY3Rpb24qKHsgQ29tcG9uZW50ICwgY3R4ICB9KSB7XG4gICAgICAgIGNvbnN0IHBhZ2VQcm9wcyA9IHlpZWxkICgwLCBfdXRpbHMpLmxvYWRHZXRJbml0aWFsUHJvcHMoQ29tcG9uZW50LCBjdHgpO1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcGFnZVByb3BzXG4gICAgICAgIH07XG4gICAgfSk7XG4gICAgcmV0dXJuIF9hcHBHZXRJbml0aWFsUHJvcHMuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbn1cbmZ1bmN0aW9uIGFwcEdldEluaXRpYWxQcm9wcyhfKSB7XG4gICAgcmV0dXJuIF9hcHBHZXRJbml0aWFsUHJvcHMuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbn1cbmNsYXNzIEFwcCBleHRlbmRzIF9yZWFjdC5kZWZhdWx0LkNvbXBvbmVudCB7XG4gICAgcmVuZGVyKCkge1xuICAgICAgICBjb25zdCB7IENvbXBvbmVudCAsIHBhZ2VQcm9wcyAgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIHJldHVybigvKiNfX1BVUkVfXyovIF9yZWFjdC5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoQ29tcG9uZW50LCBPYmplY3QuYXNzaWduKHtcbiAgICAgICAgfSwgcGFnZVByb3BzKSkpO1xuICAgIH1cbn1cbkFwcC5vcmlnR2V0SW5pdGlhbFByb3BzID0gYXBwR2V0SW5pdGlhbFByb3BzO1xuQXBwLmdldEluaXRpYWxQcm9wcyA9IGFwcEdldEluaXRpYWxQcm9wcztcbmV4cG9ydHMuZGVmYXVsdCA9IEFwcDtcblxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9X2FwcC5qcy5tYXAiXSwibmFtZXMiOlsiT2JqZWN0IiwiZGVmaW5lUHJvcGVydHkiLCJleHBvcnRzIiwidmFsdWUiLCJlbnVtZXJhYmxlIiwiZ2V0IiwiX3V0aWxzIiwiQXBwSW5pdGlhbFByb3BzIiwiTmV4dFdlYlZpdGFsc01ldHJpYyIsImRlZmF1bHQiLCJfcmVhY3QiLCJfaW50ZXJvcFJlcXVpcmVEZWZhdWx0IiwicmVxdWlyZSIsImFzeW5jR2VuZXJhdG9yU3RlcCIsImdlbiIsInJlc29sdmUiLCJyZWplY3QiLCJfbmV4dCIsIl90aHJvdyIsImtleSIsImFyZyIsImluZm8iLCJlcnJvciIsImRvbmUiLCJQcm9taXNlIiwidGhlbiIsIl9hc3luY1RvR2VuZXJhdG9yIiwiZm4iLCJzZWxmIiwiYXJncyIsImFyZ3VtZW50cyIsImFwcGx5IiwiZXJyIiwidW5kZWZpbmVkIiwib2JqIiwiX19lc01vZHVsZSIsIl9hcHBHZXRJbml0aWFsUHJvcHMiLCJDb21wb25lbnQiLCJjdHgiLCJwYWdlUHJvcHMiLCJsb2FkR2V0SW5pdGlhbFByb3BzIiwiYXBwR2V0SW5pdGlhbFByb3BzIiwiXyIsIkFwcCIsInJlbmRlciIsInByb3BzIiwiY3JlYXRlRWxlbWVudCIsImFzc2lnbiIsIm9yaWdHZXRJbml0aWFsUHJvcHMiLCJnZXRJbml0aWFsUHJvcHMiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./node_modules/next/dist/pages/_app.js\n");

/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var next_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! next/app */ \"./node_modules/next/app.js\");\n/* harmony import */ var next_app__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_app__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ \"next/head\");\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/error */ \"next/error\");\n/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_error__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ \"next/router\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var next_seo__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next-seo */ \"next-seo\");\n/* harmony import */ var next_seo__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_seo__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var utils_media__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! utils/media */ \"./utils/media.js\");\n/* harmony import */ var utils_api__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! utils/api */ \"./utils/api.js\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_7__);\n/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../store */ \"./store.js\");\n/* harmony import */ var bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.css */ \"./node_modules/bootstrap/dist/css/bootstrap.css\");\n/* harmony import */ var bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_9__);\n/* harmony import */ var _styles_fonts_css__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @/styles/fonts.css */ \"./styles/fonts.css\");\n/* harmony import */ var _styles_fonts_css__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_styles_fonts_css__WEBPACK_IMPORTED_MODULE_10__);\n/* harmony import */ var _sass_style_scss__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @/sass/style.scss */ \"./sass/style.scss\");\n/* harmony import */ var _sass_style_scss__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_sass_style_scss__WEBPACK_IMPORTED_MODULE_11__);\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_12__);\nvar _jsxFileName = \"/home/pmi-360-front2/Documentos/react-next-structure/pages/_app.js\";\n\nfunction ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }\n\nfunction _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nconst MyApp = ({\n  Component,\n  pageProps\n}) => {\n  // Extract the data we need\n  const {\n    global\n  } = pageProps;\n\n  if (global == null) {\n    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_12__.jsxDEV)((next_error__WEBPACK_IMPORTED_MODULE_2___default()), {\n      statusCode: 404\n    }, void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 21,\n      columnNumber: 12\n    }, undefined);\n  } // const { metadata } = global\n\n\n  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_12__.jsxDEV)(react_redux__WEBPACK_IMPORTED_MODULE_7__.Provider, {\n    store: _store__WEBPACK_IMPORTED_MODULE_8__.default,\n    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_12__.jsxDEV)((next_head__WEBPACK_IMPORTED_MODULE_1___default()), {}, void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 31,\n      columnNumber: 7\n    }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_12__.jsxDEV)(Component, _objectSpread({}, pageProps), void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 54,\n      columnNumber: 7\n    }, undefined)]\n  }, void 0, true, {\n    fileName: _jsxFileName,\n    lineNumber: 28,\n    columnNumber: 5\n  }, undefined);\n}; // getInitialProps disables automatic static optimization for pages that don't\n// have getStaticProps. So [[...slug]] pages still get SSG.\n// Hopefully we can replace this with getStaticProps once this issue is fixed:\n// https://github.com/vercel/next.js/discussions/10949\n\n\nMyApp.getInitialProps = async appContext => {\n  // Calls page's `getInitialProps` and fills `appProps.pageProps`\n  const appProps = await next_app__WEBPACK_IMPORTED_MODULE_0___default().getInitialProps(appContext);\n  const globalLocale = await (0,utils_api__WEBPACK_IMPORTED_MODULE_6__.getGlobalData)(appContext.router.locale);\n  return _objectSpread(_objectSpread({}, appProps), {}, {\n    pageProps: {\n      global: globalLocale\n    }\n  });\n};\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MyApp);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9fYXBwLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBOzs7QUFHQSxNQUFNUyxLQUFLLEdBQUcsQ0FBQztBQUFFQyxFQUFBQSxTQUFGO0FBQWFDLEVBQUFBO0FBQWIsQ0FBRCxLQUE4QjtBQUMxQztBQUNBLFFBQU07QUFBRUMsSUFBQUE7QUFBRixNQUFhRCxTQUFuQjs7QUFFQSxNQUFJQyxNQUFNLElBQUksSUFBZCxFQUFvQjtBQUNsQix3QkFBTywrREFBQyxtREFBRDtBQUFXLGdCQUFVLEVBQUU7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFBUDtBQUNELEdBTnlDLENBUTFDOzs7QUFFQSxzQkFFRSwrREFBQyxpREFBRDtBQUFVLFNBQUssRUFBRUosMkNBQWpCO0FBQUEsNEJBR0UsK0RBQUMsa0RBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFIRixlQTBCRSwrREFBQyxTQUFELG9CQUFlRyxTQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBMUJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQUZGO0FBK0JELENBekNELEVBMkNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQUYsS0FBSyxDQUFDSSxlQUFOLEdBQXdCLE1BQU9DLFVBQVAsSUFBc0I7QUFDNUM7QUFDQSxRQUFNQyxRQUFRLEdBQUcsTUFBTWYsK0RBQUEsQ0FBb0JjLFVBQXBCLENBQXZCO0FBQ0EsUUFBTUUsWUFBWSxHQUFHLE1BQU1WLHdEQUFhLENBQUNRLFVBQVUsQ0FBQ0csTUFBWCxDQUFrQkMsTUFBbkIsQ0FBeEM7QUFFQSx5Q0FDS0gsUUFETDtBQUVFSixJQUFBQSxTQUFTLEVBQUU7QUFDVEMsTUFBQUEsTUFBTSxFQUFFSTtBQURDO0FBRmI7QUFNRCxDQVhEOztBQWFBLGlFQUFlUCxLQUFmIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vcXVpbm4vLi9wYWdlcy9fYXBwLmpzP2Q1MzAiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEFwcCBmcm9tIFwibmV4dC9hcHBcIlxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiXG5pbXBvcnQgRXJyb3JQYWdlIGZyb20gXCJuZXh0L2Vycm9yXCJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gXCJuZXh0L3JvdXRlclwiXG5pbXBvcnQgeyBEZWZhdWx0U2VvIH0gZnJvbSBcIm5leHQtc2VvXCJcbmltcG9ydCB7IGdldFN0cmFwaU1lZGlhIH0gZnJvbSBcInV0aWxzL21lZGlhXCJcbmltcG9ydCB7IGdldEdsb2JhbERhdGEgfSBmcm9tIFwidXRpbHMvYXBpXCJcbmltcG9ydCB7IFByb3ZpZGVyIH0gZnJvbSAncmVhY3QtcmVkdXgnXG5pbXBvcnQgc3RvcmUgZnJvbSAnLi4vc3RvcmUnXG5cbmltcG9ydCAnYm9vdHN0cmFwL2Rpc3QvY3NzL2Jvb3RzdHJhcC5jc3MnXG5pbXBvcnQgXCJAL3N0eWxlcy9mb250cy5jc3NcIlxuaW1wb3J0IFwiQC9zYXNzL3N0eWxlLnNjc3NcIlxuXG5cbmNvbnN0IE15QXBwID0gKHsgQ29tcG9uZW50LCBwYWdlUHJvcHMgfSkgPT4ge1xuICAvLyBFeHRyYWN0IHRoZSBkYXRhIHdlIG5lZWRcbiAgY29uc3QgeyBnbG9iYWwgfSA9IHBhZ2VQcm9wc1xuXG4gIGlmIChnbG9iYWwgPT0gbnVsbCkge1xuICAgIHJldHVybiA8RXJyb3JQYWdlIHN0YXR1c0NvZGU9ezQwNH0gLz5cbiAgfVxuXG4gIC8vIGNvbnN0IHsgbWV0YWRhdGEgfSA9IGdsb2JhbFxuXG4gIHJldHVybiAoXG5cbiAgICA8UHJvdmlkZXIgc3RvcmU9e3N0b3JlfT5cblxuICAgICAgey8qIEZhdmljb24gKi99XG4gICAgICA8SGVhZD5cbiAgICAgICAgey8qIDxsaW5rIHJlbD1cInNob3J0Y3V0IGljb25cIiBocmVmPXtnZXRTdHJhcGlNZWRpYShnbG9iYWwuZmF2aWNvbi51cmwpfSAvPiAqL31cbiAgICAgIDwvSGVhZD5cbiAgICAgIHsvKiBHbG9iYWwgc2l0ZSBtZXRhZGF0YSAqL31cbiAgICAgIHsvKiA8RGVmYXVsdFNlb1xuICAgICAgICB0aXRsZVRlbXBsYXRlPXtgJXMgfCAke2dsb2JhbC5tZXRhVGl0bGVTdWZmaXh9YH1cbiAgICAgICAgdGl0bGU9XCJQYWdlXCJcbiAgICAgICAgZGVzY3JpcHRpb249e21ldGFkYXRhLm1ldGFEZXNjcmlwdGlvbn1cbiAgICAgICAgb3BlbkdyYXBoPXt7XG4gICAgICAgICAgaW1hZ2VzOiBPYmplY3QudmFsdWVzKG1ldGFkYXRhLnNoYXJlSW1hZ2UuZm9ybWF0cykubWFwKChpbWFnZSkgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgdXJsOiBnZXRTdHJhcGlNZWRpYShpbWFnZS51cmwpLFxuICAgICAgICAgICAgICB3aWR0aDogaW1hZ2Uud2lkdGgsXG4gICAgICAgICAgICAgIGhlaWdodDogaW1hZ2UuaGVpZ2h0LFxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pLFxuICAgICAgICB9fVxuICAgICAgICB0d2l0dGVyPXt7XG4gICAgICAgICAgY2FyZFR5cGU6IG1ldGFkYXRhLnR3aXR0ZXJDYXJkVHlwZSxcbiAgICAgICAgICBoYW5kbGU6IG1ldGFkYXRhLnR3aXR0ZXJVc2VybmFtZSxcbiAgICAgICAgfX1cbiAgICAgIC8+ICovfVxuICAgICAgey8qIERpc3BsYXkgdGhlIGNvbnRlbnQgKi99XG4gICAgICA8Q29tcG9uZW50IHsuLi5wYWdlUHJvcHN9IC8+XG4gICAgPC9Qcm92aWRlcj5cbiAgKVxufVxuXG4vLyBnZXRJbml0aWFsUHJvcHMgZGlzYWJsZXMgYXV0b21hdGljIHN0YXRpYyBvcHRpbWl6YXRpb24gZm9yIHBhZ2VzIHRoYXQgZG9uJ3Rcbi8vIGhhdmUgZ2V0U3RhdGljUHJvcHMuIFNvIFtbLi4uc2x1Z11dIHBhZ2VzIHN0aWxsIGdldCBTU0cuXG4vLyBIb3BlZnVsbHkgd2UgY2FuIHJlcGxhY2UgdGhpcyB3aXRoIGdldFN0YXRpY1Byb3BzIG9uY2UgdGhpcyBpc3N1ZSBpcyBmaXhlZDpcbi8vIGh0dHBzOi8vZ2l0aHViLmNvbS92ZXJjZWwvbmV4dC5qcy9kaXNjdXNzaW9ucy8xMDk0OVxuTXlBcHAuZ2V0SW5pdGlhbFByb3BzID0gYXN5bmMgKGFwcENvbnRleHQpID0+IHtcbiAgLy8gQ2FsbHMgcGFnZSdzIGBnZXRJbml0aWFsUHJvcHNgIGFuZCBmaWxscyBgYXBwUHJvcHMucGFnZVByb3BzYFxuICBjb25zdCBhcHBQcm9wcyA9IGF3YWl0IEFwcC5nZXRJbml0aWFsUHJvcHMoYXBwQ29udGV4dClcbiAgY29uc3QgZ2xvYmFsTG9jYWxlID0gYXdhaXQgZ2V0R2xvYmFsRGF0YShhcHBDb250ZXh0LnJvdXRlci5sb2NhbGUpXG5cbiAgcmV0dXJuIHtcbiAgICAuLi5hcHBQcm9wcyxcbiAgICBwYWdlUHJvcHM6IHtcbiAgICAgIGdsb2JhbDogZ2xvYmFsTG9jYWxlLFxuICAgIH0sXG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgTXlBcHBcbiJdLCJuYW1lcyI6WyJBcHAiLCJIZWFkIiwiRXJyb3JQYWdlIiwidXNlUm91dGVyIiwiRGVmYXVsdFNlbyIsImdldFN0cmFwaU1lZGlhIiwiZ2V0R2xvYmFsRGF0YSIsIlByb3ZpZGVyIiwic3RvcmUiLCJNeUFwcCIsIkNvbXBvbmVudCIsInBhZ2VQcm9wcyIsImdsb2JhbCIsImdldEluaXRpYWxQcm9wcyIsImFwcENvbnRleHQiLCJhcHBQcm9wcyIsImdsb2JhbExvY2FsZSIsInJvdXRlciIsImxvY2FsZSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/_app.js\n");

/***/ }),

/***/ "./reducers/blog/blogState.js":
/*!************************************!*\
  !*** ./reducers/blog/blogState.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"BlogReducer\": () => (/* binding */ BlogReducer),\n/* harmony export */   \"getBlogByCategory\": () => (/* binding */ getBlogByCategory),\n/* harmony export */   \"setBlog\": () => (/* binding */ setBlog)\n/* harmony export */ });\n/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @reduxjs/toolkit */ \"@reduxjs/toolkit\");\n/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);\n\nconst blogState = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({\n  name: \"blogState\",\n  initialState: {\n    blog: [],\n    blogByCategory: []\n  },\n  reducers: {\n    setBlog(state, {\n      payload\n    }) {\n      state.blog = payload;\n    },\n\n    getBlogByCategory(state, {\n      payload\n    }) {\n      state.blogByCategory = state.blog.filter(item => item.name === payload);\n    }\n\n  }\n});\nconst BlogReducer = blogState.reducer;\nconst {\n  getBlogByCategory,\n  setBlog\n} = blogState.actions;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZWR1Y2Vycy9ibG9nL2Jsb2dTdGF0ZS5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBO0FBRUEsTUFBTUMsU0FBUyxHQUFHRCw2REFBVyxDQUFDO0FBQzVCRSxFQUFBQSxJQUFJLEVBQUUsV0FEc0I7QUFFNUJDLEVBQUFBLFlBQVksRUFBRTtBQUNaQyxJQUFBQSxJQUFJLEVBQUUsRUFETTtBQUVaQyxJQUFBQSxjQUFjLEVBQUU7QUFGSixHQUZjO0FBTTVCQyxFQUFBQSxRQUFRLEVBQUU7QUFDUkMsSUFBQUEsT0FBTyxDQUFDQyxLQUFELEVBQVE7QUFBRUMsTUFBQUE7QUFBRixLQUFSLEVBQXFCO0FBQzFCRCxNQUFBQSxLQUFLLENBQUNKLElBQU4sR0FBYUssT0FBYjtBQUNELEtBSE87O0FBSVJDLElBQUFBLGlCQUFpQixDQUFDRixLQUFELEVBQVE7QUFBRUMsTUFBQUE7QUFBRixLQUFSLEVBQXFCO0FBQ3BDRCxNQUFBQSxLQUFLLENBQUNILGNBQU4sR0FBdUJHLEtBQUssQ0FBQ0osSUFBTixDQUFXTyxNQUFYLENBQ3BCQyxJQUFELElBQVVBLElBQUksQ0FBQ1YsSUFBTCxLQUFjTyxPQURILENBQXZCO0FBR0Q7O0FBUk87QUFOa0IsQ0FBRCxDQUE3QjtBQWtCQSxNQUFNSSxXQUFXLEdBQUdaLFNBQVMsQ0FBQ2EsT0FBOUI7QUFDQSxNQUFNO0FBQUVKLEVBQUFBLGlCQUFGO0FBQXFCSCxFQUFBQTtBQUFyQixJQUFpQ04sU0FBUyxDQUFDYyxPQUFqRCIsInNvdXJjZXMiOlsid2VicGFjazovL3F1aW5uLy4vcmVkdWNlcnMvYmxvZy9ibG9nU3RhdGUuanM/OWZkYyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge2NyZWF0ZVNsaWNlfSBmcm9tICdAcmVkdXhqcy90b29sa2l0JztcblxuY29uc3QgYmxvZ1N0YXRlID0gY3JlYXRlU2xpY2Uoe1xuICBuYW1lOiBcImJsb2dTdGF0ZVwiLFxuICBpbml0aWFsU3RhdGU6IHtcbiAgICBibG9nOiBbXSxcbiAgICBibG9nQnlDYXRlZ29yeTogW10sXG4gIH0sXG4gIHJlZHVjZXJzOiB7XG4gICAgc2V0QmxvZyhzdGF0ZSwgeyBwYXlsb2FkIH0pIHtcbiAgICAgIHN0YXRlLmJsb2cgPSBwYXlsb2FkXG4gICAgfSxcbiAgICBnZXRCbG9nQnlDYXRlZ29yeShzdGF0ZSwgeyBwYXlsb2FkIH0pIHtcbiAgICAgIHN0YXRlLmJsb2dCeUNhdGVnb3J5ID0gc3RhdGUuYmxvZy5maWx0ZXIoXG4gICAgICAgIChpdGVtKSA9PiBpdGVtLm5hbWUgPT09IHBheWxvYWRcbiAgICAgIClcbiAgICB9XG4gIH0sXG59KTtcblxuY29uc3QgQmxvZ1JlZHVjZXIgPSBibG9nU3RhdGUucmVkdWNlcjtcbmNvbnN0IHsgZ2V0QmxvZ0J5Q2F0ZWdvcnksIHNldEJsb2cgfSA9IGJsb2dTdGF0ZS5hY3Rpb25zO1xuXG5leHBvcnQgeyBCbG9nUmVkdWNlciwgZ2V0QmxvZ0J5Q2F0ZWdvcnksIHNldEJsb2cgfTsiXSwibmFtZXMiOlsiY3JlYXRlU2xpY2UiLCJibG9nU3RhdGUiLCJuYW1lIiwiaW5pdGlhbFN0YXRlIiwiYmxvZyIsImJsb2dCeUNhdGVnb3J5IiwicmVkdWNlcnMiLCJzZXRCbG9nIiwic3RhdGUiLCJwYXlsb2FkIiwiZ2V0QmxvZ0J5Q2F0ZWdvcnkiLCJmaWx0ZXIiLCJpdGVtIiwiQmxvZ1JlZHVjZXIiLCJyZWR1Y2VyIiwiYWN0aW9ucyJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./reducers/blog/blogState.js\n");

/***/ }),

/***/ "./reducers/index.js":
/*!***************************!*\
  !*** ./reducers/index.js ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ \"redux\");\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _modal_modalState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal/modalState */ \"./reducers/modal/modalState.js\");\n/* harmony import */ var _blog_blogState__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./blog/blogState */ \"./reducers/blog/blogState.js\");\n\n\n // add all reducers\n\nconst rootReducer = (0,redux__WEBPACK_IMPORTED_MODULE_0__.combineReducers)({\n  ModalReducer: _modal_modalState__WEBPACK_IMPORTED_MODULE_1__.ModalReducer,\n  BlogReducer: _blog_blogState__WEBPACK_IMPORTED_MODULE_2__.BlogReducer\n});\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (rootReducer);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZWR1Y2Vycy9pbmRleC5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBO0FBRUE7Q0FHQTs7QUFFQSxNQUFNRyxXQUFXLEdBQUdILHNEQUFlLENBQUM7QUFDbENDLEVBQUFBLFlBRGtDO0FBRWxDQyxFQUFBQSxXQUFXQSwwREFBQUE7QUFGdUIsQ0FBRCxDQUFuQztBQUtBLGlFQUFlQyxXQUFmIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vcXVpbm4vLi9yZWR1Y2Vycy9pbmRleC5qcz9jZThlIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Y29tYmluZVJlZHVjZXJzfSBmcm9tICdyZWR1eCdcblxuaW1wb3J0IHtNb2RhbFJlZHVjZXJ9IGZyb20gJy4vbW9kYWwvbW9kYWxTdGF0ZSdcbmltcG9ydCB7QmxvZ1JlZHVjZXJ9IGZyb20gJy4vYmxvZy9ibG9nU3RhdGUnXG5cbi8vIGFkZCBhbGwgcmVkdWNlcnNcblxuY29uc3Qgcm9vdFJlZHVjZXIgPSBjb21iaW5lUmVkdWNlcnMoe1xuICBNb2RhbFJlZHVjZXIsXG4gIEJsb2dSZWR1Y2VyLFxufSk7XG5cbmV4cG9ydCBkZWZhdWx0IHJvb3RSZWR1Y2VyIl0sIm5hbWVzIjpbImNvbWJpbmVSZWR1Y2VycyIsIk1vZGFsUmVkdWNlciIsIkJsb2dSZWR1Y2VyIiwicm9vdFJlZHVjZXIiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./reducers/index.js\n");

/***/ }),

/***/ "./reducers/modal/modalState.js":
/*!**************************************!*\
  !*** ./reducers/modal/modalState.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"ModalReducer\": () => (/* binding */ ModalReducer),\n/* harmony export */   \"setState\": () => (/* binding */ setState)\n/* harmony export */ });\n/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @reduxjs/toolkit */ \"@reduxjs/toolkit\");\n/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);\n\nconst modalState = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({\n  name: 'modalState',\n  initialState: {\n    isActive: false\n  },\n  reducers: {\n    setState(state, action) {\n      state.isActive = action.payload;\n    }\n\n  }\n});\nconst ModalReducer = modalState.reducer;\nconst {\n  setState\n} = modalState.actions;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZWR1Y2Vycy9tb2RhbC9tb2RhbFN0YXRlLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTtBQUVBLE1BQU1DLFVBQVUsR0FBR0QsNkRBQVcsQ0FBQztBQUM3QkUsRUFBQUEsSUFBSSxFQUFFLFlBRHVCO0FBRTdCQyxFQUFBQSxZQUFZLEVBQUU7QUFDWkMsSUFBQUEsUUFBUSxFQUFFO0FBREUsR0FGZTtBQUs3QkMsRUFBQUEsUUFBUSxFQUFFO0FBQ1JDLElBQUFBLFFBQVEsQ0FBQ0MsS0FBRCxFQUFRQyxNQUFSLEVBQWdCO0FBQ3RCRCxNQUFBQSxLQUFLLENBQUNILFFBQU4sR0FBaUJJLE1BQU0sQ0FBQ0MsT0FBeEI7QUFDRDs7QUFITztBQUxtQixDQUFELENBQTlCO0FBWUEsTUFBTUMsWUFBWSxHQUFHVCxVQUFVLENBQUNVLE9BQWhDO0FBQ0EsTUFBTTtBQUFFTCxFQUFBQTtBQUFGLElBQWVMLFVBQVUsQ0FBQ1csT0FBaEMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9xdWlubi8uL3JlZHVjZXJzL21vZGFsL21vZGFsU3RhdGUuanM/NTUwYyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge2NyZWF0ZVNsaWNlfSBmcm9tICdAcmVkdXhqcy90b29sa2l0J1xuXG5jb25zdCBtb2RhbFN0YXRlID0gY3JlYXRlU2xpY2Uoe1xuICBuYW1lOiAnbW9kYWxTdGF0ZScsXG4gIGluaXRpYWxTdGF0ZToge1xuICAgIGlzQWN0aXZlOiBmYWxzZSxcbiAgfSxcbiAgcmVkdWNlcnM6IHtcbiAgICBzZXRTdGF0ZShzdGF0ZSwgYWN0aW9uKSB7XG4gICAgICBzdGF0ZS5pc0FjdGl2ZSA9IGFjdGlvbi5wYXlsb2FkO1xuICAgIH1cbiAgfVxufSlcblxuY29uc3QgTW9kYWxSZWR1Y2VyID0gbW9kYWxTdGF0ZS5yZWR1Y2VyXG5jb25zdCB7IHNldFN0YXRlIH0gPSBtb2RhbFN0YXRlLmFjdGlvbnNcblxuZXhwb3J0IHsgTW9kYWxSZWR1Y2VyLCBzZXRTdGF0ZSB9OyJdLCJuYW1lcyI6WyJjcmVhdGVTbGljZSIsIm1vZGFsU3RhdGUiLCJuYW1lIiwiaW5pdGlhbFN0YXRlIiwiaXNBY3RpdmUiLCJyZWR1Y2VycyIsInNldFN0YXRlIiwic3RhdGUiLCJhY3Rpb24iLCJwYXlsb2FkIiwiTW9kYWxSZWR1Y2VyIiwicmVkdWNlciIsImFjdGlvbnMiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./reducers/modal/modalState.js\n");

/***/ }),

/***/ "./store.js":
/*!******************!*\
  !*** ./store.js ***!
  \******************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"store\": () => (/* binding */ store),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @reduxjs/toolkit */ \"@reduxjs/toolkit\");\n/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _reducers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reducers */ \"./reducers/index.js\");\n\n\nconst store = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.configureStore)({\n  reducer: _reducers__WEBPACK_IMPORTED_MODULE_1__.default\n});\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (store);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zdG9yZS5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBO0FBQ0E7QUFFTyxNQUFNRSxLQUFLLEdBQUdGLGdFQUFjLENBQUM7QUFDbENHLEVBQUFBLE9BQU8sRUFBRUYsOENBQVdBO0FBRGMsQ0FBRCxDQUE1QjtBQUlQLGlFQUFlQyxLQUFmIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vcXVpbm4vLi9zdG9yZS5qcz8wNTcxIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGNvbmZpZ3VyZVN0b3JlIH0gZnJvbSBcIkByZWR1eGpzL3Rvb2xraXRcIjtcbmltcG9ydCByb290UmVkdWNlciBmcm9tICcuL3JlZHVjZXJzJ1xuXG5leHBvcnQgY29uc3Qgc3RvcmUgPSBjb25maWd1cmVTdG9yZSh7XG4gIHJlZHVjZXI6IHJvb3RSZWR1Y2VyLFxufSk7XG5cbmV4cG9ydCBkZWZhdWx0IHN0b3JlO1xuIl0sIm5hbWVzIjpbImNvbmZpZ3VyZVN0b3JlIiwicm9vdFJlZHVjZXIiLCJzdG9yZSIsInJlZHVjZXIiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./store.js\n");

/***/ }),

/***/ "./utils/api.js":
/*!**********************!*\
  !*** ./utils/api.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"getStrapiURL\": () => (/* binding */ getStrapiURL),\n/* harmony export */   \"fetchAPI\": () => (/* binding */ fetchAPI),\n/* harmony export */   \"getPageData\": () => (/* binding */ getPageData),\n/* harmony export */   \"getGlobalData\": () => (/* binding */ getGlobalData)\n/* harmony export */ });\nfunction ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }\n\nfunction _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\nfunction getStrapiURL(path) {\n  return `${\"http://18.218.43.130:1337\" || 0 // return process.env.NEXT_PUBLIC_STRAPI_API_URL || 'http://18.117.176.110:1337'\n  }${path}`;\n} // Helper to make GET requests to Strapi\n\nasync function fetchAPI(path, options = {}) {\n  const defaultOptions = {\n    headers: {\n      'Content-Type': 'application/json'\n    }\n  };\n\n  const mergedOptions = _objectSpread(_objectSpread({}, defaultOptions), options);\n\n  const requestUrl = getStrapiURL(path);\n  const response = await fetch(requestUrl, mergedOptions);\n\n  if (!response.ok) {\n    console.error(response.statusText);\n    throw new Error(`An error occured please try again`);\n  }\n\n  const data = await response.json();\n  return data;\n}\n/**\n *\n * @param {object} params The router params object with slug: { slug: [<slug>] }\n * @param {string} locale The current locale specified in router.locale\n * @param {boolean} preview router isPreview value\n */\n\nasync function getPageData(params, locale, preview) {\n  const slug = params.slug.join('/'); // Find the pages that match this slug\n\n  const pagesData = await fetchAPI(`/pages?slug=${slug}&_locale=${locale}&status=published${preview ? '&status=draft' : ''}`); // Make sure we found something, otherwise return null\n\n  if (pagesData == null || pagesData.length === 0) {\n    return null;\n  } // Return the first item since there should only be one result per slug\n\n\n  return pagesData[0];\n} // Get site data from Strapi (metadata, navbar, footer...)\n\nasync function getGlobalData(locale) {\n  const global = await fetchAPI(`/global?_locale=${locale}`);\n  return global;\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi91dGlscy9hcGkuanMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUFPLFNBQVNBLFlBQVQsQ0FBc0JDLElBQXRCLEVBQTRCO0FBQ2pDLFNBQVEsR0FBRUMsMkJBQUEsSUFBMEMsQ0FBN0MsQ0FDTDtBQUNDLEtBQUVELElBQUssRUFGVjtBQUdELEVBRUQ7O0FBQ08sZUFBZUksUUFBZixDQUF3QkosSUFBeEIsRUFBOEJLLE9BQU8sR0FBRyxFQUF4QyxFQUE0QztBQUNqRCxRQUFNQyxjQUFjLEdBQUc7QUFDckJDLElBQUFBLE9BQU8sRUFBRTtBQUNQLHNCQUFnQjtBQURUO0FBRFksR0FBdkI7O0FBS0EsUUFBTUMsYUFBYSxtQ0FDZEYsY0FEYyxHQUVkRCxPQUZjLENBQW5COztBQUlBLFFBQU1JLFVBQVUsR0FBR1YsWUFBWSxDQUFDQyxJQUFELENBQS9CO0FBRUEsUUFBTVUsUUFBUSxHQUFHLE1BQU1DLEtBQUssQ0FBQ0YsVUFBRCxFQUFhRCxhQUFiLENBQTVCOztBQUVBLE1BQUksQ0FBQ0UsUUFBUSxDQUFDRSxFQUFkLEVBQWtCO0FBQ2hCQyxJQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY0osUUFBUSxDQUFDSyxVQUF2QjtBQUNBLFVBQU0sSUFBSUMsS0FBSixDQUFXLG1DQUFYLENBQU47QUFDRDs7QUFDRCxRQUFNQyxJQUFJLEdBQUcsTUFBTVAsUUFBUSxDQUFDUSxJQUFULEVBQW5CO0FBQ0EsU0FBT0QsSUFBUDtBQUNEO0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNPLGVBQWVFLFdBQWYsQ0FBMkJDLE1BQTNCLEVBQW1DQyxNQUFuQyxFQUEyQ0MsT0FBM0MsRUFBb0Q7QUFDekQsUUFBTUMsSUFBSSxHQUFHSCxNQUFNLENBQUNHLElBQVAsQ0FBWUMsSUFBWixDQUFpQixHQUFqQixDQUFiLENBRHlELENBRXpEOztBQUNBLFFBQU1DLFNBQVMsR0FBRyxNQUFNckIsUUFBUSxDQUM3QixlQUFjbUIsSUFBSyxZQUFXRixNQUFPLG9CQUFtQkMsT0FBTyxHQUFHLGVBQUgsR0FBcUIsRUFDcEYsRUFGNkIsQ0FBaEMsQ0FIeUQsQ0FRekQ7O0FBQ0EsTUFBSUcsU0FBUyxJQUFJLElBQWIsSUFBcUJBLFNBQVMsQ0FBQ0MsTUFBVixLQUFxQixDQUE5QyxFQUFpRDtBQUMvQyxXQUFPLElBQVA7QUFDRCxHQVh3RCxDQWF6RDs7O0FBQ0EsU0FBT0QsU0FBUyxDQUFDLENBQUQsQ0FBaEI7QUFDRCxFQUVEOztBQUNPLGVBQWVFLGFBQWYsQ0FBNkJOLE1BQTdCLEVBQXFDO0FBQzFDLFFBQU1PLE1BQU0sR0FBRyxNQUFNeEIsUUFBUSxDQUFFLG1CQUFrQmlCLE1BQU8sRUFBM0IsQ0FBN0I7QUFFQSxTQUFPTyxNQUFQO0FBQ0QiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9xdWlubi8uL3V0aWxzL2FwaS5qcz9jYjMwIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBmdW5jdGlvbiBnZXRTdHJhcGlVUkwocGF0aCkge1xuICByZXR1cm4gYCR7cHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfU1RSQVBJX0FQSV9VUkwgfHwgJ2h0dHA6Ly8xOC4xMTcuMTc2LjExMDoxMzM3J1xuICAgIC8vIHJldHVybiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19TVFJBUElfQVBJX1VSTCB8fCAnaHR0cDovLzE4LjExNy4xNzYuMTEwOjEzMzcnXG4gICAgfSR7cGF0aH1gXG59XG5cbi8vIEhlbHBlciB0byBtYWtlIEdFVCByZXF1ZXN0cyB0byBTdHJhcGlcbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBmZXRjaEFQSShwYXRoLCBvcHRpb25zID0ge30pIHtcbiAgY29uc3QgZGVmYXVsdE9wdGlvbnMgPSB7XG4gICAgaGVhZGVyczoge1xuICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICB9LFxuICB9XG4gIGNvbnN0IG1lcmdlZE9wdGlvbnMgPSB7XG4gICAgLi4uZGVmYXVsdE9wdGlvbnMsXG4gICAgLi4ub3B0aW9ucyxcbiAgfVxuICBjb25zdCByZXF1ZXN0VXJsID0gZ2V0U3RyYXBpVVJMKHBhdGgpXG5cbiAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBmZXRjaChyZXF1ZXN0VXJsLCBtZXJnZWRPcHRpb25zKVxuXG4gIGlmICghcmVzcG9uc2Uub2spIHtcbiAgICBjb25zb2xlLmVycm9yKHJlc3BvbnNlLnN0YXR1c1RleHQpXG4gICAgdGhyb3cgbmV3IEVycm9yKGBBbiBlcnJvciBvY2N1cmVkIHBsZWFzZSB0cnkgYWdhaW5gKVxuICB9XG4gIGNvbnN0IGRhdGEgPSBhd2FpdCByZXNwb25zZS5qc29uKClcbiAgcmV0dXJuIGRhdGFcbn1cblxuLyoqXG4gKlxuICogQHBhcmFtIHtvYmplY3R9IHBhcmFtcyBUaGUgcm91dGVyIHBhcmFtcyBvYmplY3Qgd2l0aCBzbHVnOiB7IHNsdWc6IFs8c2x1Zz5dIH1cbiAqIEBwYXJhbSB7c3RyaW5nfSBsb2NhbGUgVGhlIGN1cnJlbnQgbG9jYWxlIHNwZWNpZmllZCBpbiByb3V0ZXIubG9jYWxlXG4gKiBAcGFyYW0ge2Jvb2xlYW59IHByZXZpZXcgcm91dGVyIGlzUHJldmlldyB2YWx1ZVxuICovXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0UGFnZURhdGEocGFyYW1zLCBsb2NhbGUsIHByZXZpZXcpIHtcbiAgY29uc3Qgc2x1ZyA9IHBhcmFtcy5zbHVnLmpvaW4oJy8nKVxuICAvLyBGaW5kIHRoZSBwYWdlcyB0aGF0IG1hdGNoIHRoaXMgc2x1Z1xuICBjb25zdCBwYWdlc0RhdGEgPSBhd2FpdCBmZXRjaEFQSShcbiAgICBgL3BhZ2VzP3NsdWc9JHtzbHVnfSZfbG9jYWxlPSR7bG9jYWxlfSZzdGF0dXM9cHVibGlzaGVkJHtwcmV2aWV3ID8gJyZzdGF0dXM9ZHJhZnQnIDogJydcbiAgICB9YFxuICApXG5cbiAgLy8gTWFrZSBzdXJlIHdlIGZvdW5kIHNvbWV0aGluZywgb3RoZXJ3aXNlIHJldHVybiBudWxsXG4gIGlmIChwYWdlc0RhdGEgPT0gbnVsbCB8fCBwYWdlc0RhdGEubGVuZ3RoID09PSAwKSB7XG4gICAgcmV0dXJuIG51bGxcbiAgfVxuXG4gIC8vIFJldHVybiB0aGUgZmlyc3QgaXRlbSBzaW5jZSB0aGVyZSBzaG91bGQgb25seSBiZSBvbmUgcmVzdWx0IHBlciBzbHVnXG4gIHJldHVybiBwYWdlc0RhdGFbMF1cbn1cblxuLy8gR2V0IHNpdGUgZGF0YSBmcm9tIFN0cmFwaSAobWV0YWRhdGEsIG5hdmJhciwgZm9vdGVyLi4uKVxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldEdsb2JhbERhdGEobG9jYWxlKSB7XG4gIGNvbnN0IGdsb2JhbCA9IGF3YWl0IGZldGNoQVBJKGAvZ2xvYmFsP19sb2NhbGU9JHtsb2NhbGV9YClcblxuICByZXR1cm4gZ2xvYmFsXG59XG4iXSwibmFtZXMiOlsiZ2V0U3RyYXBpVVJMIiwicGF0aCIsInByb2Nlc3MiLCJlbnYiLCJORVhUX1BVQkxJQ19TVFJBUElfQVBJX1VSTCIsImZldGNoQVBJIiwib3B0aW9ucyIsImRlZmF1bHRPcHRpb25zIiwiaGVhZGVycyIsIm1lcmdlZE9wdGlvbnMiLCJyZXF1ZXN0VXJsIiwicmVzcG9uc2UiLCJmZXRjaCIsIm9rIiwiY29uc29sZSIsImVycm9yIiwic3RhdHVzVGV4dCIsIkVycm9yIiwiZGF0YSIsImpzb24iLCJnZXRQYWdlRGF0YSIsInBhcmFtcyIsImxvY2FsZSIsInByZXZpZXciLCJzbHVnIiwiam9pbiIsInBhZ2VzRGF0YSIsImxlbmd0aCIsImdldEdsb2JhbERhdGEiLCJnbG9iYWwiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./utils/api.js\n");

/***/ }),

/***/ "./utils/media.js":
/*!************************!*\
  !*** ./utils/media.js ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"getStrapiMedia\": () => (/* binding */ getStrapiMedia)\n/* harmony export */ });\nfunction getStrapiMedia(url) {\n  if (url == null) {\n    return null;\n  } // Return the full URL if the media is hosted on an external provider\n\n\n  if (url.startsWith(\"http\") || url.startsWith(\"//\")) {\n    return url;\n  } // Otherwise prepend the URL path with the Strapi URL\n\n\n  return `${\"http://18.218.43.130:1337\" || 0}${url}`;\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi91dGlscy9tZWRpYS5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7O0FBQU8sU0FBU0EsY0FBVCxDQUF3QkMsR0FBeEIsRUFBNkI7QUFDbEMsTUFBSUEsR0FBRyxJQUFJLElBQVgsRUFBaUI7QUFDZixXQUFPLElBQVA7QUFDRCxHQUhpQyxDQUtsQzs7O0FBQ0EsTUFBSUEsR0FBRyxDQUFDQyxVQUFKLENBQWUsTUFBZixLQUEwQkQsR0FBRyxDQUFDQyxVQUFKLENBQWUsSUFBZixDQUE5QixFQUFvRDtBQUNsRCxXQUFPRCxHQUFQO0FBQ0QsR0FSaUMsQ0FVbEM7OztBQUNBLFNBQVEsR0FDTkUsMkJBQUEsSUFBMEMsQ0FDM0MsR0FBRUYsR0FBSSxFQUZQO0FBR0QiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9xdWlubi8uL3V0aWxzL21lZGlhLmpzP2E3YzciXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGZ1bmN0aW9uIGdldFN0cmFwaU1lZGlhKHVybCkge1xuICBpZiAodXJsID09IG51bGwpIHtcbiAgICByZXR1cm4gbnVsbFxuICB9XG5cbiAgLy8gUmV0dXJuIHRoZSBmdWxsIFVSTCBpZiB0aGUgbWVkaWEgaXMgaG9zdGVkIG9uIGFuIGV4dGVybmFsIHByb3ZpZGVyXG4gIGlmICh1cmwuc3RhcnRzV2l0aChcImh0dHBcIikgfHwgdXJsLnN0YXJ0c1dpdGgoXCIvL1wiKSkge1xuICAgIHJldHVybiB1cmxcbiAgfVxuXG4gIC8vIE90aGVyd2lzZSBwcmVwZW5kIHRoZSBVUkwgcGF0aCB3aXRoIHRoZSBTdHJhcGkgVVJMXG4gIHJldHVybiBgJHtcbiAgICBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19TVFJBUElfQVBJX1VSTCB8fCBcImh0dHA6Ly9sb2NhbGhvc3Q6MTMzN1wiXG4gIH0ke3VybH1gXG59XG4iXSwibmFtZXMiOlsiZ2V0U3RyYXBpTWVkaWEiLCJ1cmwiLCJzdGFydHNXaXRoIiwicHJvY2VzcyIsImVudiIsIk5FWFRfUFVCTElDX1NUUkFQSV9BUElfVVJMIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./utils/media.js\n");

/***/ }),

/***/ "./node_modules/bootstrap/dist/css/bootstrap.css":
/*!*******************************************************!*\
  !*** ./node_modules/bootstrap/dist/css/bootstrap.css ***!
  \*******************************************************/
/***/ (() => {



/***/ }),

/***/ "./sass/style.scss":
/*!*************************!*\
  !*** ./sass/style.scss ***!
  \*************************/
/***/ (() => {



/***/ }),

/***/ "./styles/fonts.css":
/*!**************************!*\
  !*** ./styles/fonts.css ***!
  \**************************/
/***/ (() => {



/***/ }),

/***/ "@reduxjs/toolkit":
/*!***********************************!*\
  !*** external "@reduxjs/toolkit" ***!
  \***********************************/
/***/ ((module) => {

"use strict";
module.exports = require("@reduxjs/toolkit");

/***/ }),

/***/ "next-seo":
/*!***************************!*\
  !*** external "next-seo" ***!
  \***************************/
/***/ ((module) => {

"use strict";
module.exports = require("next-seo");

/***/ }),

/***/ "../shared/lib/utils":
/*!************************************************!*\
  !*** external "next/dist/shared/lib/utils.js" ***!
  \************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ "next/error":
/*!*****************************!*\
  !*** external "next/error" ***!
  \*****************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/error");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/***/ ((module) => {

"use strict";
module.exports = require("react-redux");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("redux");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/_app.js"));
module.exports = __webpack_exports__;

})();